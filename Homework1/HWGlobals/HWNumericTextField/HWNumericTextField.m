//
//  HWNumericTextField.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWNumericTextField.h"

@implementation HWNumericTextField

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.keyboardType = UIKeyboardTypeNumberPad;
        self.borderStyle = UITextBorderStyleRoundedRect;
        self.tintColor = [UIColor hw_colorWithHexInt:HWLeafColorHex];
        self.returnKeyType = UIReturnKeyDone;
    }
    return self;
}

@end
