//
//  NSString+HWSize.m
//  HelpWill
//
//  Created by Viktor Peschenkov on 05.07.16.
//  Copyright © 2016 INOSTUDIO. All rights reserved.
//

#import "NSString+HWSize.h"

@implementation NSString (HWSize)

- (CGSize)hw_sizeWithFont:(UIFont *)font
        constrainedToSize:(CGSize)size
            lineBreakMode:(NSLineBreakMode)lineBreakMode {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;
    NSDictionary *attributes = @{
                                 NSFontAttributeName : font,
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return [self boundingRectWithSize:size
                              options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                           attributes:attributes
                              context:nil].size;
}

- (CGSize)hw_sizeWithAttributes:(NSDictionary *)attributes
              constrainedToSize:(CGSize)size {
    return [self boundingRectWithSize:size
                              options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                           attributes:attributes
                              context:nil].size;
}

- (CGSize)hw_sizeWithAttributes:(NSDictionary *)attributes
                       forWidth:(CGFloat)width {
    return [self boundingRectWithSize:(CGSize){ width, CGFLOAT_MAX }
                              options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                           attributes:attributes
                              context:nil].size;
}

- (CGSize)hw_sizeWithFont:(UIFont *)font
                 forWidth:(CGFloat)width
            lineBreakMode:(NSLineBreakMode)lineBreakMode {
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName : font,
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return [self boundingRectWithSize:(CGSize){ width, CGFLOAT_MAX }
                              options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                           attributes:attributes
                              context:nil].size;
}

- (CGSize)hw_sizeWithFont:(UIFont *)font
        constrainedToSize:(CGSize)size {
    NSDictionary *attributes = @{ NSFontAttributeName : font };
    return [self boundingRectWithSize:size
                              options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                           attributes:attributes
                              context:nil].size;
}

- (CGSize)hw_sizeWithFont:(UIFont *)font {
    return [self sizeWithAttributes:@{ NSFontAttributeName : font }];
}

@end
